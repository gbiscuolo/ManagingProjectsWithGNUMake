#!/usr/bin/make -f
default: all

########################################################################
# Bootstrap:
PACKAGE.NAME = ManagingProjectsWithGNUMake
PACKAGE.VERSION = 3.1.4
include ./code/ShakeNMake/shake-n-make.make
CLEAN_FILES += *~
DISTCLEAN_FILES += $(PACKAGE.NAME)-$(PACKAGE.VERSION)*
# i don't want the Makefile in this dist tarbal but ShakeNMake adds it
# automatically:
PACKAGE.DIST_FILES := $(filter-out Makefile,$(PROJECT.DIST_FILES))
# /Bootstrap
########################################################################

########################################################################
# Administrative:
doc.master = ManagingProjectsWithGNUMake-3.1.x.odt
# /Administrative
########################################################################
# ODT
doc.odt = ManagingProjectsWithGNUMake-$(PACKAGE.VERSION).odt
$(doc.odt): $(doc.master)
	cp $^ $@
odt: $(doc.odt)
docs: odt
CLEAN_FILES += $(doc.odt)
# /ODT
########################################################################
# PDF:
doc.master.pdf := $(wildcard $(subst .odt,.pdf,$(doc.master)))
doc.pdf := $(subst .odt,.pdf,$(doc.odt))
ifneq '' '$(doc.master.pdf)'
    # Assume that $(doc.master.pdf) was exported from OOo directly
    # from $(doc.odt) (this is the usual case).
    $(doc.master.pdf): $(doc.master.odt)
	@echo -e "************************************************************\n" \
		"WARNING:\n\t$@\nis older than:\n\t$^\nYou need to manually re-export it!\n" \
	      "************************************************************"
    $(doc.pdf): $(doc.master.pdf)
	cp $^ $@
    DISTCLEAN_FILES += $(doc.master.pdf)
else
    $(doc.pdf): $(doc.odt)
	@echo "i don't know how to auto-convert ODT to PDF. Please use OpenOffice to export $(doc.odt) to $(doc.pdf)"
	@exit 1
endif#$(doc.pdf) rules
pdf: $(doc.pdf)
docs: pdf
CLEAN_FILES += $(doc.pdf)
# /PDF
########################################################################
# DIST:
dist-hook:# make sure that the code subdirs don't have extra files:
	cd code/ShakeNMake && $(MAKE) --no-print-directory distclean
	cd code/rmoo && rm -f ./*~ ./#*
	cd code/jgc && rm -f ./*~ ./#*
	@for x in code/*/Makefile; do d=$${x%%/Makefile}; make -C $$d clean; done
	@true
dist: docs dist-hook
PACKAGE.DIST_FILES += $(doc.odt) $(doc.pdf) \
	code/ShakeNMake \
	code/rmoo \
	code/jgc \
	code/examples
# /DIST
########################################################################

all: docs
