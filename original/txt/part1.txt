PART I
I.

Basic Concepts

In Part I, we focus on the features of make, what they do, and how to use them properly. We begin with a brief introduction and overview that should be enough to get you started on your first makefile. The chapters then cover make rules, variables, functions, and finally command scripts. When you are finished with Part I, you will have a fairly complete working knowledge of GNU make and have many advanced usages well in hand.


