# Managing Projects with GNU Make, 3.1.4 Edition

This is a derivative work by [Giovanni Biscuolo](mailto:g ReplaceWithAt xelera.eu), based on the 2007 derivative work by Stephan Beal, of the book ``Managing Projects with GNU Make, Third Edition`` by Robert Mecklenburg and published by O'Reilly & Associates, Inc. in 2004 under the terms of GNU Free Documentation License (FDL).

For the full copyright statement and license text, please see section 1.1 of the book.

## History

In 2004 O'Reilly & Associates, Inc. published [Managing Projects with GNU Make, Third Edition](http://www.oreilly.com/catalog/make3/book/) by Robert Mecklenburg under the terms of GNU Free Documentation License (FDL). O'Reilly's version of the book technically follows the word of the FDL but their distribution format makes modification and derivation of the document extremely difficult as they only publish it as a series of 22 PDF files, one file per document section/chapter.

In 2007 [Stephan Beal](http://wanderinghorse.net/home/stephan/) published his [derivative work](http://wanderinghorse.net/computing/make/), with the primary purpose to create a document which is truly open, to allow for portability, extendibility, and ease of future maintenance. He made four editions of his book: 3.1.1 to 3.1.4, last released on 28 June 2007. He lately made further unpublished changes until July 8 2009; see ``git log`` for the detailed changes history.

In November 2017 I started this derivative work with the primary purpose to convert the source code of the book in [reStructuredText](http://docutils.sourceforge.net/rst.html) and use [Sphinx documentation generator](http://www.sphinx-doc.org) to publish it in one or more of it's supported output format, using [git](https://git-scm.com/) as it's primary version control system. I hope this will allow better portability, extendibility and ease of maintainance than the ODT SVN versioned one.

## SVN to git migration

I converted Stephan Beal SNV repository using [svn2git](https://github.com/nirvdrum/svn2git) ver. 2.4.0 [packaged in Debian Stretch](https://packages.debian.org/stretch/svn2git) with this command: ``svn2git https://svn.code.sf.net/p/toc/svn/make-book/``

