
#include <iostream>

#include <dlfcn.h>

#ifndef COUT
#define COUT std::cout << __FILE__ << ":" << std::dec << __LINE__ << " : "
#endif
#ifndef CERR
#define CERR std::cerr << __FILE__ << ":" << std::dec << __LINE__ << " : "
#endif

#include "refcount.hpp"

/**
   A finalizer type for use with refcount::rcptr<>.
*/
struct dlclose_finalizer
{

	/** Gets call by rcptr when the ref count for soh
	    goes to 0.
	*/
	void operator()( void * & soh )
	{
		if( soh )
		{
			COUT << "rcptr is closing dll "<<std::hex<<soh<<" ..." << std::endl;
			dlclose( soh );
			COUT << "rcptr Closed dll @ " << std::hex<<soh<< std::endl;
		}
	}
};
typedef refcount::rcptr<void,dlclose_finalizer> soh_ptr;

/** A sentry so we can see right when main() is entered/exited. */
struct Last2Go
{
public:
	Last2Go()
	{
		COUT << "Last2Go() main() is starting right now.\n";
	}
	~Last2Go()
	{
		COUT << "~Last2Go() main() should exit right about ... n-\n";
	}

};

int main( int argc, char ** argv )
{
	if( 1 == argc )
	{
		CERR << "Usage: " << argv[0] << " dll_1.so [... dll_N.so]"<<std::endl;
		return 1;
	}
	Last2Go sentry;
	soh_ptr mainsoh( dlopen( 0, RTLD_NOW | RTLD_GLOBAL ) );
	{
		for( int i = 1; i < argc; ++i )
		{
			char const * filename = argv[i];
			COUT << "Opening dll "<<filename<<" ...\n";
			soh_ptr soh( dlopen( filename, RTLD_NOW | RTLD_GLOBAL ) );
			if( ! soh.get() )
			{
				CERR << "Could not dlopen("<<filename<<"). dlerror: " << dlerror()<<"\n";
				return 2;
			}
			COUT << "Opened dll @ " << std::hex << soh.get() << '\n';
			//soh.take(); // leak handle, so it will close after
			//main() exits. This is unpredictable and means
			//callbacks from the DLL will likely fail.
			
			//soh.take(0); // close DLL right now.

			// or let soh pass out of scope...
		}
	}
	COUT << "Exiting app ...\n";
	return 0;
}
