#include <iostream>

#ifndef COUT
#define COUT std::cout << __FILE__ << ":" << std::dec << __LINE__ << " : "
#endif

#ifndef THIS_DLL_NAME
#  define THIS_DLL_NAME "[[{{<<Unnamed>>}}]].DLL"
#endif

namespace {
	struct DLLSentry
	{
		DLLSentry()
		{
			COUT << THIS_DLL_NAME << ": DLL static init phase.\n";
		}
		~DLLSentry()
		{
			COUT << THIS_DLL_NAME << ": DLL static destruction phase.\n";
		}
	};
	static DLLSentry astruct = DLLSentry();

	void silly_function(std::string const & msg)
	{
		COUT << THIS_DLL_NAME<<": silly_function(\""<<msg<<"\")"<<std::endl;
	}
	
	static int init = (silly_function("Called during static init"),0);
}
