########################################################################
# $(call remove-dupes,list)
# Returns a string equal to list with duplicate entries removed. It does
# not sort the list.
#
### Implementation attempt #1:
#remove-dupes-holder :=
#remove-dupes-impl = $(if $(filter $(1),$(remove-dupes-holder)),,\
#			$(eval remove-dupes-holder+=$1))
#remove-dupes = $(strip \
#	$(eval remove-dupes-holder:=)\
#	$(foreach ENTRY,\
#		$(if $1,$1,),\
#		$(call remove-dupes-impl,$(ENTRY)))\
#	$(remove-dupes-holder))
### Implementation #2, contributed by Christoph Schulz
remove-dupes = $(if $1,$(strip $(word 1,$1) $(call $0,$(filter-out $(word 1,$1),$1))))

ifeq (1,1)
remove-dupes-test:
	# remove-dupes: $(call remove-dupes,c b a a b c)
	# remove-dupes: <$(call remove-dupes,a b A a b B c a c c b d C)>
	# remove-dupes: <$(call remove-dupes,$(strip ))>
#	# remove-dupes: <$(call remove-dupes)>
else
all:
endif
