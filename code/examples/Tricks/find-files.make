########################################################################
# $(call find-files,wildcard-pattern,path)
# Returns a string containing ALL files or dirs in the given path
# which match the wildcard pattern. The path may be colon- or
# space-delimited. Spaces in path elements or filenames is of
# course evil and will Cause Grief. If either $1 or $2 are empty
# then $(error) is called.
find-files = $(wildcard			\
                 $(addsuffix /$(if $1,$1,$(error find-files requires a wildcard argument for $$1)),		\
                     $(subst :, ,		\
                       $(subst ::,:.:,		\
                         $(patsubst :%,.:%,	\
                           $(patsubst %:,%:.,$(if $2,$2,$(error find-files ($$1=$1) requires a PATH as arg $$2))))))))
########################################################################
# $(call find-programs,wildcard[,path])
# Returns a list of ALL files/dirs matching the given wildcard in the
# given path (default=$(PATH)).
find-programs = $(call find-files,$1,$(if $2,$2,$(PATH)))
########################################################################
# $(call find-file,wildcard-pattern,path)
# Returns a string containing the FIRST file in the given path
# which matches the wildcard pattern. If the path is empty or not
# given then $(error) is called.
find-file = $(firstword $(call find-files,$1,$2))
########################################################################
# $(call find-program,wildcard[,path])
# Returns the FIRST file/dir matching the given wildcard in the given
# path (default=$(PATH)).
find-program = $(firstword $(call find-programs,$1,$2))


ifeq (1,1)
find-files-test:
	# find-files: $(call find-files,*profile,$(HOME):/etc)
	# find-file: $(call find-file,Desktop,$(HOME))
	# find-programs: $(call find-programs,wc)
	# find-program: $(call find-program,gcc,/opt/gcc/current/bin:$(PATH))
	# find-programs: $(call find-programs,gcc,/opt/gcc/current/bin:$(PATH))
#	# find-files error: $(call find-files,test-error)
#	# find-files error: $(call find-files)
else
all:
endif
