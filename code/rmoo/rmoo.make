#!/do/not/make -f
# ^^^^ to force emacs to think this is a Makefile
########################################################################
# Version: 2007.06.07
#
# This file contains Make code for simulating OO-style objects in
# Make. It originally appeared in Robert Mecklenburg's "Managing
# Projects with GNU Make, Third Edition", and was presumably authored
# by Robert.
#
# Stephan Beal added documentation and renamed the functions/vars
# to improve encapsulation and help avoid potential collisions with
# client-side makefiles.
#
########################################################################
# Example usage:
#
#############
# include rmoo.make
## Define a struct called MyStruct:
# $(call rmoo-define,MyStruct,
#                    $(call rmoo-slot,slotFoo,default value),        
#                    $(call rmoo-slot,slotBar,default value)
# )
#
# Create an instance of MyStruct:
# obj1 := $(call rmoo-new,MyStruct)
# $(call rmoo-set,obj1,slotFoo,a value)
# $(call rmoo-set,obj2,slotBar,another value)
#
# Show the values of obj1's slotFoo slotBar:
# foo:
#      @echo 'obj1::slotFoo = $(call rmoo-get,obj1,slotFoo)'
#      @echo 'obj1::slotBar = $(call rmoo-get,obj1,slotBar)'
#############
#
# Most misuse of this code will result in $(error) being called.
#
########################################################################
# Code naming conventions:
#
# Variables are named rmoo-var_name and functions are named
# rmoo-func-name.
#
# All variables and functions in this file prefixed with "rmoo-." are
# private to this file and not intended for client-side use. Those
# prefixed with "rmoo-" (without a period) are considered part of
# the public API and may be used by client-side makefiles. That said,
# client-side code should never alter any of the rmoo variables.
#
# "rmoo" is a dual-meaning acronym for:
#
#   1) Robert Mecklenburg's Object Orientedness
#   2) Robert's Make[file] Object Orientedness
#
# and if you want to stretch the imagination a bit it could also mean:
#
#   3) Real Makers obJECT [to] OBjects
#
# But #3 is really dependent on vocal influctions to gets its
# point across.
#
# This acronym was selected over "defstruct" (the original name)
# because: a) to honor the original author, b) it's highly unlikely to
# be used in client makefiles, c) it's easier type than "defstruct".
#
# Senseless trivia: using an English/Querty keyboard layout,
# "defstruct" is typed almost entirely with the fingers of the left
# hand (except for the letter "u"), and therebye encourages repetative
# stress syndrome in the left hand (as if we don't have enough of that
# already). Conversely, "rmoo" follows a more comfortable pattern for
# the fingers.
#
########################################################################
# LEGALITIES:
#
# As this code was derived from a work licensed under the GNU Free
# Documentation License (FDL), this file is distributed under the same
# license.
#
# In blatant violation of the FDL, i am not including a copy of the
# whole FDL license text in this document (because it would increas
# the size by 10 times). Instead, for the full text please visit:
#
#              http://www.gnu.org/copyleft/fdl.txt
#
# But here's the full license statement, for you nitpickets out there:
#
# Permission is granted to copy, distribute, and/or modify this
# document under the terms of the GNU Free Documentation License,
# Version 1.1 or any later version published by the Free Software
# Foundation; with no Invariant Sections, no Front-Cover Texts, and no
# Back-Cover Texts. A copy of this license can be obtained at
# http://www.gnu.org/copyleft/fdl.txt.
#
# For purposes of the GNU FDL, this file has the following copyright
# holders:
#
# Copyright (C) 2004 O'Reilly & Associates, Inc.
#                    http://www.oreilly.com/catalog/make3/book/
#
# Copyright (C) 2007 Stephan Beal
#                    http://wanderinghorse.net/home/stephan/
#
# The home page of this code is:
#
#              http://wanderinghorse.net/computing/make/
#
########################################################################


########################################################################
# $(rmoo-all_structs) - a list of the defined structure names. Client
# code may look at this, but please do not change it.
rmoo-all_structs :=
# $(rmoo-all_instances) - a list of all the instances of all
# structures. Client code may look at this, but please do not change
# it.
rmoo-all_instances :=


########################################################################
# $(rmoo-.next-id) - return a unique identifier by appending a
# single character to an internal string and returning the word count.
# Usage: $(eval TOKEN$(rmoo-.next-id))
rmoo-.next_id_counter :=# internal use only
define rmoo-.next-id
$(words $(rmoo-.next_id_counter))$(eval rmoo-.next_id_counter += 1)
endef


########################################################################
# $(call rmoo-define, struct_name,
#        $(call rmoo-slot, slot_name, value),
#        ...)
# Defines a new "struct" named struct_name, containing a number of
# "slots" (effectively "member variables") created by $(call
# rmoo-slot). It accepts up to 10 slots per struct. If you
# need more, then tweak this code to suit.
#
# Sample usage:
# $(call rmoo-define,MyStruct,
#        $(call rmoo-slot,slot1,value of slot1),
#        $(call rmoo-slot,slot2,42)
#  )
#
# Client code should not use any variables with a prefix which is the
# same as a struct's name. Using that prefix may inadvertently collide
# with internal variables defined by this code.
define rmoo-define
  $(eval rmoo-all_structs += $1) 					\
  $(eval $(1)_def_slotnames :=)						\
  $(foreach v, $2 $3 $4 $5 $6 $7 $8 $9 $(10) $(11),			\
    $(if $($v_name),							\
      $(eval $(1)_def_slotnames          += $($v_name))			\
      $(eval $(1)_def_$($v_name)_default := $($v_value))))
endef

########################################################################
# $(call rmoo-slot,slot_name,slot_value)
# Creates a "slot" (member variable) for a rmoo-defined object.
# Intended to be used only as the 2nd (and subsequent) argument(s) to
# $(call rmoo-define,MyStruct,$(call rmoo-slot,...),...)
define rmoo-slot
#  $(eval tmp_id := $(next_id))# logic error in the original code? next_id is never defined
#  $(eval $(1)_$(tmp_id)_name := $1)
#  $(eval $(1)_$(tmp_id)_value := $2)
# Following two lines changed by stephan:
  $(eval $(1)_name := $1)
  $(eval $(2)_value := $2)
# and the change makes no difference in the runtime behaviour :?
#  $(1)_$(tmp_id)
endef


########################################################################
# $(call rmoo-new, struct_name)
#
# Creates a new "instance" of the "type" named by struct_name. If
# called before $(call rmoo-define,struct_name,...) is called then
# it produces an error.
#
# Sample usage:
#  myobject := $(call rmoo-new,MyStruct)
#
# Limitation: all instances of objects must have a unique name,
# regardless of what struct_name they belong to. Since make has no
# data types except string, this can be considered a "limitation
# of the environment", and not an inherent flaw in the rmoo
# mechanism.
define rmoo-new
$(strip								\
  $(if $(filter $1,$(rmoo-all_structs)),,			\
    $(error rmoo-new on unknown struct '$(strip $1)'))	\
  $(eval rmoo-.instance := $1@$(rmoo-.next-id))	\
  $(eval rmoo-all_instances += $(rmoo-.instance))	\
  $(foreach v, $($(strip $1)_def_slotnames),			\
    $(eval $(rmoo-.instance)_$v := $($(strip $1)_def_$v_default)))	\
  $(rmoo-.instance))
endef

########################################################################
# $(call rmoo-delete, instance_name)
# "Deletes" an instance of a struct created with $(call rmoo-new,...)
# If the instance does not exist then $(error) is called.
#
# Sample usage:
# $(call rmoo-delete,myobject)
define rmoo-delete
$(strip								 \
  $(if $(filter $($(strip $1)),$(rmoo-all_instances)),,	 \
    $(error Invalid instance '$($(strip $1))'))			 \
  $(eval rmoo-all_instances := $(filter-out $($(strip $1)),$(rmoo-all_instances))) \
  $(foreach v, $($(strip $1)_def_slotnames),			 \
    $(eval $(rmoo-.instance)_$v := )))
endef

########################################################################
# $(call rmoo-.struct-name, instance_id)
# Extracts an object's struct name from the encoded instance ID.
define rmoo-.struct-name
$(firstword $(subst @, ,$($(strip $1))))
endef

########################################################################
# $(call rmoo-.check-params, instance_id, slot_name)
# Does some sanity checking on the parameters, ensuring that an object
# instance with the given instance_id exists and that it has the
# given slot.
# On error in called $(error)
define rmoo-.check-params
  $(if $(filter $($(strip $1)),$(rmoo-all_instances)),,		\
    $(error Invalid instance '$(strip $1)'))			\
  $(if $(filter $2,$($(call rmoo-.struct-name,$1)_def_slotnames)),,	\
    $(error Instance '$($(strip $1))' does not have slot '$(strip $2)'))
endef

########################################################################
# $(call rmoo-get, instance_id, slot_name)
# Returns the (stripped) value of a given slot value from an object.
#
# Sample usage:
#   foo := $(call rmoo-get,myobject,slot1)
define rmoo-get
$(strip				\
  $(call rmoo-.check-params,$1,$2) 	\
  $($($(strip $1))_$(strip $2)))
endef

########################################################################
# $(call rmoo-set, instance_id, slot_name, value)
# Sets a slot value on an object.
# If the object or slot do not exist then $(error) is called.
#
# Sample usage:
# $(call rmoo-set,mybobject,slot1,a value)
define rmoo-set
  $(call rmoo-.check-params,$1,$2) \
  $(eval $($(strip $1))_$(strip $2) := $3)
endef

########################################################################
# $(call rmoo-dump-struct, struct_name)
#
# A debugging function to dump info about ALL instances of a given
# structure's slots and their values.
#
# Sample usage:
#     echo $(call rmoo-dump-struct,MyStruct)
define rmoo-dump-struct
{ $(strip $1)_def_slotnames "$($(strip $1)_def_slotnames)" 	\
  $(foreach s,							\
    $($(strip $1)_def_slotnames),$(strip 			\
    $(strip $1)_def_$s_default "$($(strip $1)_def_$s_default)")) }
endef

########################################################################
# $(call print-struct, struct_name)
#
# A debugging function to dump info about slot values of ALL instances
# of a given structure. The difference between this and
# rmoo-dump-struct is that the latter shows slot names, whereas
# this one shows only values.
#
# Sample usage:
#     echo $(call rmoo-print-struct,MyStruct)
define print-struct
{ $(foreach s,					\
    $($(strip $1)_def_slotnames),$(strip	\
    { "$s" "$($(strip $1)_def_$s_default)" })) }
endef

########################################################################
# $(call rmoo-dump, instance_id)
# A debugging function to dump slot/value information for a single
# instance of a struct.
#
# Sample usage:
# echo $(call rmoo-dump,myobject)
define rmoo-dump
{ $(eval tmp_name := $(call rmoo-.struct-name,$1)) 	\
  $(foreach s,					\
    $($(tmp_name)_def_slotnames),$(strip 	\
    { $($(strip $1))_$s "$($($(strip $1))_$s)" })) }
endef

########################################################################
# $(call rmoo-print, instance_id)
# A debugging function to dump slot/value information for a single
# instance of a struct. The difference between this function and
# rmoo-dump is that the latter shows slot names, whereas
# this one only shows values.
#
# Sample usage:
# echo $(call rmoo-print,myobject)
define rmoo-print
{ $(foreach s,						\
    $($(call rmoo-.struct-name,$1)_def_slotnames),"$(strip 	\
    $(call rmoo-get,$1,$s))") }
endef
